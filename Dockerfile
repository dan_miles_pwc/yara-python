FROM ubuntu:latest
LABEL maintainer="PwC Cyber Threat Ops - daniel.miles@pwc.com"

ARG YARA_VERSION=4.0.2
ARG YARA_PYTHON_VERSION=4.0.2
ARG DEBIAN_FRONTEND="noninteractive"

WORKDIR /
COPY requirements.txt requirements.txt

# Update packages
RUN apt-get -qq update && \
# Install build packages
    apt-get install --no-install-recommends -qq \
    automake \
    build-essential \
    curl \
    gcc \
    git \
    libtool \
    make \
    pkg-config \
    libssl-dev \
    libjansson-dev \
    python3-dev \
    python3-pip \
    python3-wheel \
    python3-setuptools \
    libmagic-dev \
    libmagic-dev \
    libarchive-dev \
    libfuzzy-dev \
    python3-lxml \
    libxml2-dev \
    libxslt-dev \
    libimage-exiftool-perl && \
# Install pip requirements
    pip3 install -r /requirements.txt && \
# Install YARA
    cd /tmp/ && \
    curl -OL https://github.com/VirusTotal/yara/archive/v$YARA_VERSION.tar.gz && \
    tar -zxvf v$YARA_VERSION.tar.gz && \
    cd yara-$YARA_VERSION/ && \
    ./bootstrap.sh && \
    ./configure --with-crypto --enable-dotnet --enable-magic --enable-cuckoo && \
    make && make install && make check && \
# Install yara-python
    cd /tmp/ && \
    curl -OL https://github.com/VirusTotal/yara-python/archive/v$YARA_PYTHON_VERSION.tar.gz && \
    tar -zxvf v$YARA_PYTHON_VERSION.tar.gz && \
    cd yara-python-$YARA_PYTHON_VERSION/ && \
    python3 setup.py build --dynamic-linking && \
    python3 setup.py install && \
# Remove build packages
    python3 setup.py -q clean --all && \
    apt-get autoremove -qq --purge \
    automake \
    build-essential \
    curl \
    gcc \
    git \
    libtool \
    make \
    python3-dev \
    python3-wheel && \
    apt-get purge -qq python3-setuptools  && \
    apt-get clean -qq
